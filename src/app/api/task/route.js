import { Task } from "../../../models/task"
import connectDataBase from "../../../utils/database"

export async function POST(request) {
    connectDataBase()
    const { name, date, description, image, avatar } = await request.json()
    try {
        const post = new Task({ name, date, description, image, avatar })
        console.log(post)
        await post.save()
        return Response.json({
            data: { name, date, description, image, avatar },
            message: "Post successfully created"
        })
    } catch (err) {
        return Response.json(err, { status: 500 })
    }

}

export async function GET() {
    connectDataBase();
    try {
        const posts = await Task.find();
        return Response.json(posts, { status: 201 })
    } catch (err) {
        return Response.json(err)
    }
}
