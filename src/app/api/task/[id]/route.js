import { Task } from "../../../../models/task";
import connectDataBase from "../../../../utils/database";

export async function GET(request, { params }) {
    await connectDataBase();
    try {
        const post = await Task.findById(params.id)
        if (!post.id) {
            Response.json("Post not defined with this id", { status: 500 })
        }
        return Response.json(post, { status: 201 })
    } catch (err) {
        Response.json("Get one post filed", { status: 500 })
    }
}

export async function DELETE() {
    await connectDataBase();
    try {
        const post = await Task.deleteOne({ _id: params.id })
        return Response.json("post successfully deleted", { status: 201 })
    } catch (err) {
        Response.json("Get one post filed", { status: 500 })
    }
}

export async function PATCH() {
    await connectDataBase();
    const { name, date, description, image, avatar } = await request.json()
    try {
        const post = await Task.findById({ _id: params.id }).exec()

        if (!post.id) {
            Response.json("Post not defined with this id", { status: 500 })
        }

        blog.name = name
        blog.date = date
        blog.description = description
        blog.image = image
        blog.avatar = avatar
        await post.save()
        return Response.json("post successfully edited", { status: 201 })
    } catch (err) {
        Response.json("Get one post filed", { status: 500 })
    }
}