import mongoose from "mongoose"

async function connectDataBase() {
    try {
        await mongoose.connect(process.env.MONGODB_URL, {
            dbName: "my_Task",
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
    } catch (err) {
        console.log(err)
    }
}
export default connectDataBase;