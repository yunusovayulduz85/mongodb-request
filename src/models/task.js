import mongoose, { Schema, models } from "mongoose";

const TaskBlogSchema = new Schema({
    name: String,
    date: String,
    description: String,
    image: String,
    avatar: String,
})

export const Task = models.Task || mongoose.model("Task", TaskBlogSchema)